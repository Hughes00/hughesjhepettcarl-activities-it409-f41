<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="_token"  content="<?php echo csrf_token() ?>"/>

    <title>Hello, world!</title>
</head>

<body>
    <form>
        <input id="testval" name="testval" placeholder="Write here...">
        
        <button id="send" onclick="sendPost()" type="button">Send</button>
    </form>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="<?php echo asset('js/testval.js') ?>"></script>
</body>

</html>
