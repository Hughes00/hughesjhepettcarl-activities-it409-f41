<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://unpkg.com/vue@next"></script>
    <title>Music Player</title>
</head>

<body>
    <div id="app">
        <div class="sidebar color-theme" :class="{showSidebar: sidebarToggled}">
            <div class="sidebar-logo">
                <i class="fas fa-window-close d-lg-none d-block" id="hide-sidebar" @click="hideSidebar"></i>
                <img src="img/logo.png" class="img-fluid p-5" alt="Music Logo">
            </div>
            <div class="sidebar-menu">
                <ul>
                    <li @click="adjustModal('upload')">
                        <a>
                            <i class="fas fa-cloud-upload-alt"></i>
                            <span>Upload Music</span>
                        </a>
                    </li>
                    <li>
                        <a @click="getSongs">
                            <i class="fas fa-music"></i>
                            <span>All Songs</span>
                        </a>
                    </li>
                </ul>

                <ul id="create-playlist">
                    <li @click="adjustModal('create')">
                        <a>
                            <i class="fas fa-folder-plus"></i>
                            <span>Create Playlist</span>
                        </a>
                    </li>
                </ul>
                <ul id="playlist">
                    
                    <li v-for="(playlist, index) in playlists" class="row m-0 align-items-center">
                        <a class="px-0" @click="showPlaylistSongs(index)">
                            <i class="fas fa-list-ul"></i>
                            <span>{{playlist.playlist}}</span>
                        </a>
                        <div class="playlist_dropdown">
                            <div class="dropdown">
                                <span class="fas fa-ellipsis-v fa-sm px-1" :id="index" data-bs-toggle="dropdown" aria-expanded="false" ></span>
                                <ul class="dropdown-menu dropdown-menu-dark" :aria-labelledby="index">
                                    <li @click="showPlaylistModalSongs(index)"><a class="dropdown-item">Add Song</a></li>
                                    <li class="pt-0"><hr class="dropdown-divider"></li>
                                    <li @click="showEditPlaylistModal(index)"><a class="dropdown-item">Edit</a></li>
                                    <li @click="deleteConfirmation(index, 'playlist')" class="pt-0 pb-3"><a class="dropdown-item">Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="content">
            <div class="container-fluid px-0 h-100">
                <div id="show-sidebar" class="d-lg-none d-block"><i class="fas fa-bars" @click="showSidebar"></i></div>
                <input class="fomr-control mt-4 p-1 pl-2" placeholder="Search" v-model="searchQuery">
                <div id="player">
                    <h2>
                        {{headerText}}
                    </h2>
                    <div class="table-responsive tbl-responsive">
                        <table class="table">
                            <thead class="color-theme">
                                <tr>
                                    <th> </th>
                                    <th>Title</th>
                                    <th>Artist</th>
                                    <th>Album</th>
                                    <th>Duration</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr><td colspan="6" class="empty" v-if="resultQuery.length==0">No audio found.</td></tr>
                                <tr v-for="(song, index) in resultQuery">
                                    <td valign="middle" class="text-center"><i class="fas fa-play"></i></td>
                                    <td valign="middle">{{song.title}}</td>
                                    <td valign="middle">{{song.artist}}</td>
                                    <td valign="middle">{{song.album}}</td>
                                    <td valign="middle">{{song.duration}}</td>
                                    <td valign="middle" align="center" style="width: 50px"><button @click="deleteConfirmation(index, 'song')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="controller">
                <div class="d-flex align-items-center position-relative">
                    <div id="pointer"></div>
                    <div id="progress_bar">
                        <div id="progress"></div>
                    </div>
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    <i class="fas fa-backward"></i>
                    <i class="fas fa-play"></i>
                    <i class="fas fa-forward"></i>
                </div>
            </div>
        </div>

        <div class="modal fade" id="upload-create-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header color-theme">
                        <h5 class="modal-title" id="exampleModalLabel">{{modalHeader}}</h5>
                    </div>
                    <div class="modal-body">
                        <div v-show="modalName=='upload'">
                            <div class="text-center drag-drop py-4 px-5" @dragover="dragOver" @dragenter="dragFiles" @dragleave="dragLeaveFiles" @drop="drop">
                                <label for="file" v-if="fileList.length==0">Drag and drop files here</label>
                                <audio id="audio" preload="metadata" style="display: none"></audio>
                                <input name="file" id="file" type="file" multiple style="display: none" @change="onFileChange" />
                                <div class="col-12">
                                    <div class="file-item row m-0 my-3" v-for="(file, index) in fileList">
                                        <small v-if="checkFiles(index)===false">Sorry we only allow audio files.</small>
                                        <div class="col-10 text-truncate text-start" :class="{'file-item-error': checkFiles(index)===false}">{{file.name}}</div>
                                        <div class="col-2"> <i class="fas fa-times-circle" @click="removeFile(index)"></i></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <input type="text" id="playlist_name" v-show="modalName=='create' || modalName=='edit'" v-model="playlistNameInput">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="pos" @click="validate">{{modalButton}}</button>
                        <button type="button" class="neg" data-bs-dismiss="modal" @click="cancelModal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addSongPlaylistModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="addSongPlaylistModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                <div class="modal-header color-theme">
                    <h4 class="modal-title" id="staticBackdropLabel">Add Song to {{modalHeaderPlaylist}}</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive tbl-responsive p-4">
                        <table class="w-100">
                            <tbody>
                               <tr><td colspan="6" class="empty" v-if="notInPlaylistSong.length==0">No available audio.</td></tr>
                                <tr class="modal-song" v-for="(song, index) in notInPlaylistSong">
                                    <td class="py-2">{{song.title}}</td>
                                    <td class="py-2" align="center" valign="middle" style="width: 50px"><button @click="addSongToPlaylist(index)" class="btn btn-outline-success btn-sm"><i class="fas fa-plus"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr class="mb-0">
                <div class="modal-footer">
                    <button type="button" class="neg" data-bs-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmation-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header color-theme">
                        <h5 class="modal-title" id="exampleModalLabel">{{confirmationModalHeader}}</h5>
                    </div>
                    <div class="modal-body">
                        <p>{{deletebody}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="pos" @click="deleteItems">{{deleteBtn}}</button>
                        <button type="button" class="neg" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="js/jsmediatags.min.js"></script>
    <script src="js/main.js"></script>
</body></html>
