<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Song;
use App\Models\Playlist;
use App\Models\Playlist_song;

class SongController extends Controller
{
    public function insertSong(){
        //error_log(request('title'));

        $song = new Song();
        $song->title = request('title');
        $song->length = request('length');
        $song->artist = request('artist');
        //$song->album = request('album');
        $song->save();
        
        $last_id = $song->id;
        request()->validate([
            'song' => 'nullable|file|mimes:audio/mpeg,mpga,mp3,wav,aac'
        ]);

        $fileName = $last_id.'-song.'.request()->song->getClientOriginalExtension();
        request()->song->move(public_path('audios'), $fileName);
        return $last_id;
    }
    //songs
    public function displaySong(){
        $song = Song::all();
        return $song;
    }
    public function deleteSong(){
        $id = request('id');
        Song::where('id', $id)->delete();
        Playlist_song::where('song_id', $id)->delete();

        $audio_path = public_path('audios/' . $id . "-song");
        $ext = "";
        if(file_exists($audio_path.".mp3")){
            $ext = ".mp3";
        }
        else if(file_exists($audio_path.".wav")){
            $ext = ".wav";
        }
        else if(file_exists($audio_path.".aac")){
            $ext = ".aac";
        }
        else if(file_exists($audio_path.".mpeg")){
            $ext = ".mpeg";
        }
        else if(file_exists($audio_path.".mpga")){
            $ext = ".mpga";
        }
        unlink($audio_path.$ext);
        return;
    }

    public function insertPlaylist(){
        $playlist = new Playlist();
        $playlist->name = request('playlistname');
        $playlist->save();
        return $playlist->id;
    }
    public function displayPlaylist(){
        $playlist = Playlist::all();
        return $playlist;
    }
    public function editPlaylist(){
        $id = request('id');
        $name = request('name');
        Playlist::where('id', $id)->update(['name'=>$name]);
        return;
    }
    public function deletePlaylist(){
        Playlist::where('id', request('id'))->delete();
        Playlist_song::where('playlist_id', request('id'))->delete();
        return;
    }



    public function displayPlaylistSongs($id){
        
        //$results = DB::select('SELECT songs.id, title, length, artist, playlist_songs.id as playlist_songs_id FROM songs, playlist_songs WHERE playlist_songs.song_id = songs.id and playlist_songs.playlist_id=?', ['2']);
       /* $song = DB::table('songs','playlist_songs')
                    ->where('playlist_songs.song_id', '=', 'songs.id')
                    ->where('playlist_songs.playlist_id', '=', '2')
                    ->select('songs.id','title','length','artist', 'playlist_songs.id as playlist_songs_id')
                    ->get();
                    error_log($song);*/
        
        $song = Song::select('songs.id', 'title', 'length', 'artist', 'playlist_songs.id as playlist_songs_id','playlist_songs.playlist_id')
        ->leftJoin('playlist_songs', 'playlist_songs.song_id', '=', 'songs.id')
        ->where('playlist_songs.playlist_id', '=', $id)
        ->get();



        //SELECT songs.`id`, `title`, `length`, `artist`, playlist_songs.id as playlist_songs_id, playlist_songs.playlist_id FROM `songs` LEFT JOIn playlist_songs on playlist_songs.song_id = songs.id;


        return $song;


        //SELECT songs.id, title, length, artist, playlist_songs.id as playlist_songs_id FROM songs, playlist_songs WHERE playlist_songs.song_id = songs.id and playlist_songs.playlist_id = 2
    }
    public function getNotInPlaylistSong($id){
        $playlist_song = Playlist_song::where('playlist_id', '=', $id)->pluck('song_id')->all();
        
        $song = Song::whereNotIn('id', $playlist_song)->get();

        /*$song = Song::select('songs.id', 'title', 'length', 'artist', 'playlist_songs.id as playlist_songs_id','playlist_songs.playlist_id')
        ->leftJoin('playlist_songs', 'playlist_songs.song_id', '=', 'songs.id')
        ->where('playlist_songs.playlist_id', '!=', $id)
        ->get();*/

        return $song;
    }
    

    
    public function addSongToPlaylist(){
        $playlist_song = new Playlist_song();
        $playlist_song->song_id = request('song_id');
        $playlist_song->playlist_id = request('playlist_id');
        $playlist_song->save();
        return $playlist_song->id;
    }
    public function deletesongtoplaylist(){
        $playlist_song = Playlist_song::where('id', request('id'))->delete();
        error_log(request('id'));
        return;
    }













    public function testCon(){
        
        error_log(request('title'));
        error_log(request('artist'));
        error_log(request('album'));
        error_log(request('length'));
        return request('title');
    }
    
}
