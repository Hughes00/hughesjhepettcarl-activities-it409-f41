<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//songs
Route::post('/addsong', [SongController::class, 'insertSong']);
Route::post('/deleteSong', [SongController::class, 'deleteSong']);
Route::get('/displaysong', [SongController::class, 'displaySong']);

//playlist
Route::post('/addplaylist', [SongController::class, 'insertPlaylist']);
Route::get('/displayplaylist', [SongController::class, 'displayPlaylist']);
Route::post('/editplaylist', [SongController::class, 'editPlaylist']);
Route::post('/deleteplaylist', [SongController::class, 'deletePlaylist']);
Route::get('/displayPlaylistSongs/{id}', [SongController::class, 'displayPlaylistSongs']);
Route::get('/getNotInPlaylistSong/{id}', [SongController::class, 'getNotInPlaylistSong']);
//Add song to playlist
Route::post('/addsongtoplaylist', [SongController::class, 'addSongToPlaylist']);
Route::post('/deletesongtoplaylist', [SongController::class, 'deletesongtoplaylist']);
Route::post('/test', [SongController::class, 'testCon']);




Route::get('/test', function(){
    return view('test');
});

Route::get('/', function(){
    return view('musicplayer');
});
