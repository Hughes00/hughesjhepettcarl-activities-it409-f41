const app = Vue.createApp({
    data() {
        return {
            fileList: [],
            sidebarToggled: false,
            headerText: "All Songs",
            myModal: null,
            modalName: "",
            modalHeader: "",
            modalButton: "",
            searchQuery: "",
            modalHeaderPlaylist: "",
            modalPlaylistId: "",
            playlistNameInput: "",
            editPlaylistIndex: "",
            confirmationModalHeader: "",
            deleteTarget: "",
            deleteItem: "",
            deletebody: "",
            deleteBtn: "",
            index: "",
            currentTab: "",
            notInPlaylistSong: [],
            playlists: [],
            songs: []
        };
    },
    methods: {
        showSidebar() {
            this.sidebarToggled = true;
        },
        hideSidebar() {
            this.sidebarToggled = false;
        },
        adjustModal(modalName) {
            this.modalName = modalName;
            if (modalName == "upload") {
                this.modalHeader = "Upload"
                this.modalButton = "Upload"
            } 
            else if (modalName == "create") {
                this.modalHeader = "Create Playlist"
                this.modalButton = "Create"
            }
            else if (modalName == "edit") {
                this.modalHeader = "Edit Playlist"
                this.modalButton = "Save"
            }
            this.myModal = new bootstrap.Modal(document.getElementById('upload-create-modal'), {})
            this.myModal.show()
        },
        onFileChange(e) {
            this.saveToArray(e.target.files);
        },
        saveToArray(file) {
            for (i = 0; i < file.length; i++) {
                this.fileList.push(file[i]);
            }
        },
        checkFiles(i) {
            if (this.fileList[i].type.match('audio.*')) {
                return true;
            } else {
                return false;
            }
        },
        dragFiles(e) {
            e.preventDefault()
            e.target.classList.add('dragging');
        },
        dragLeaveFiles(e) {
            e.target.classList.remove('dragging');
        },
        dragOver(e) {
            e.preventDefault();
        },
        drop(e) {
            e.preventDefault();
            e.target.classList.remove('dragging');
            var fileList = e.dataTransfer.files;
            this.saveToArray(fileList);
        },
        removeFile(i) {
            this.fileList.splice(i, 1);
        },
        createPlaylist() {
            var playlistname = this.playlistNameInput;

            var formData = new FormData();
            formData.append('playlistname', playlistname);

            axios.post('/addplaylist',
                formData
            ).then(({ data })=> {
                this.playlists.push({
                    id: data,
                    playlist: playlistname
                });
            }).catch((err)=> {});

            setTimeout(() => {
                this.playlistNameInput = ""
            }, 100);
            this.hideModal();
        },
        cancelModal() {
            if (this.modalName == "upload") {
                setTimeout(() => {
                    this.fileList = [];
                }, 100);
            } else if (this.modalName == "create" || this.modalName == "edit") {
                this.playlistNameInput = "";
            }
        },
        hideModal(){
            this.myModal.hide();
        },
        convertDuration(sec) {
            var h = this.setMinimumIntegerDigits(Math.floor(sec / 3600));
            var m = this.setMinimumIntegerDigits(Math.floor(sec % 3600 / 60));
            var s = this.setMinimumIntegerDigits(Math.floor(sec % 3600 % 60));
            var hms = h + ":" + m + ":" + s;
            var ms = m + ":" + s;
            if (h == "00") {
                return ms;
            } else {
                return hms;
            }
        },
        setMinimumIntegerDigits(t) {
            return t.toLocaleString('en-US', {
                minimumIntegerDigits: 2,
                useGrouping: false
            });
        },
        uploadAudio() {
            var jsmediatags = window.jsmediatags;
            var file = this.fileList;
            var vue = this;
            if (file.length != 0) {
                if (this.fileList[0].type.match('audio.*')) {
                    jsmediatags.read(file[0], {
                        onSuccess: function (tag) {
                            var tags = tag.tags;
                            var title = '';
                            var artist = 'None';
                            var album = 'None';
                            var duration = '';

                            if (tags.title) {
                                title = tags.title;
                            } else {
                                var t = file[0].name.split('.').slice(0, -1);
                                title = t[0];
                            }
                            if (tags.artist) {
                                artist = tags.artist;
                            }
                            if (tags.album) {
                                album = tags.album;
                            }
                            var src = URL.createObjectURL(file[0]);
                            var audio = document.querySelector('audio');
                            audio.onloadedmetadata = () => {
                                vue.saveToDatabase(title, artist, album, audio.duration, file[0]);
                                file.splice(0, 1);
                                vue.uploadAudio();
                            }
                            audio.src = src;
                            vue.hideModal();
                        },
                        onError: function (error) {
                            console.log(error);
                        }
                    });
                } else {
                    file.splice(0, 1);
                    vue.uploadAudio();
                    vue.hideModal();
                }
            }
        },
        validate() {
            if (this.modalName == "upload") {
                this.uploadAudio();
            }
            else if (this.modalName == "create") {
                this.createPlaylist();
            }
            else if (this.modalName == "edit") {
                this.editPlaylist();
            }
        },
        showEditPlaylistModal(i){
            this.editPlaylistIndex = i;
            this.playlistNameInput = this.playlists[i].playlist;
            this.adjustModal("edit");
        },
        editPlaylist(){
            var i = this.editPlaylistIndex;
            var id = this.playlists[i].id;
            var name = this.playlistNameInput;

            var vue = this;
            var formData = new FormData();
            formData.append('id', id);
            formData.append('name', name);
            axios.post('/editplaylist',
                formData
            ).then(({ data })=> {
                vue.playlists[i].playlist = name;
            }).catch((err)=> {});
            setTimeout(() => {
                this.playlistNameInput = ""
            }, 100);
            this.hideModal();
        },
        deleteConfirmation(i, target){
            this.index = i;
            this.deleteTarget = target;
            var name = "";
            if(target == "song"){
                name = this.songs[i].title;
                this.confirmationModalHeader = "Delete Song";
                this.deletebody = "Are you sure you want to delete "+name+"?";
                this.deleteBtn = "Delete";

                if(this.modalPlaylistId != ""){
                    name = this.songs[i].title;
                    this.confirmationModalHeader = "Remove Song";
                    this.deletebody = "Are you sure you want to remove "+name+"?";
                    this.deleteBtn = "Remove";
                }
            }
            else if(target == "playlist"){
                name = this.playlists[i].playlist;
                this.confirmationModalHeader = "Delete Playlist";
                this.deletebody = "Are you sure you want to delete "+name+"?";
                this.deleteBtn = "Delete";
            }
            this.myModal = new bootstrap.Modal(document.getElementById('confirmation-modal'), {})
            this.myModal.show()
        },
        deleteItems(){ 
            
            var i = this.index;
            console.log(i);
            if(this.deleteTarget == "song"){
                var endpoint = '/deleteSong';
                var id = this.songs[i].id;
                if(this.modalPlaylistId != ""){
                    endpoint = 'deletesongtoplaylist';
                    id = this.songs[i].playlist_songs_id;
                }
                this.sendDeleteAxios(id, endpoint);
            }
            else if(this.deleteTarget == "playlist"){
                var endpoint = '/deleteplaylist';
                var id = this.playlists[i].id;
                this.sendDeleteAxios(id, endpoint);
            }
        },
        sendDeleteAxios(id, endpoint){
            var vue = this;
            var formData = new FormData();
            formData.append('id', id);
            axios.post(endpoint,
                formData
            ).then(({ data })=> {
                if(vue.deleteTarget == "song"){
                    vue.songs.splice(vue.index, 1);
                }
                else if(vue.deleteTarget == "playlist"){
                    vue.playlists.splice(vue.index, 1);
                    vue.getSongs()
                }
                vue.hideModal()
            }).catch((err)=> {});
            
        },
        showPlaylistModalSongs(i){
            var vue = this;
            this.modalPlaylistId = this.playlists[i].id;
            this.modalHeaderPlaylist = this.playlists[i].playlist;
            this.notInPlaylistSong = [];
            var id = this.playlists[i].id;
            axios.get('/getNotInPlaylistSong/'+id).then(({ data })=> {
                for(i=0; i < data.length; i++){
                    vue.notInPlaylistSong.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: 'none',
                        duration: vue.convertDuration(data[i].length)
                    });
                }
                this.myModal = new bootstrap.Modal(document.getElementById('addSongPlaylistModal'), {})
                this.myModal.show()
            }).catch((err)=> {});
        },
        addSongToPlaylist(i){
            var vue = this;
            var formData = new FormData();
            formData.append('song_id', vue.notInPlaylistSong[i].id);
            formData.append('playlist_id', vue.modalPlaylistId);
            axios.post('/addsongtoplaylist',
                formData
            ).then(({ data })=> {
                if(vue.modalPlaylistId == vue.currentTab){
                    vue.songs.push({
                        id: vue.notInPlaylistSong[i].id,
                        title: vue.notInPlaylistSong[i].title,
                        artist: vue.notInPlaylistSong[i].artist,
                        album: vue.notInPlaylistSong[i].album,
                        duration: vue.notInPlaylistSong[i].duration, 
                        playlist_songs_id: data
                    });
                }
                vue.notInPlaylistSong.splice(i, 1);
            }).catch((err)=> {});
        },
        showPlaylistSongs(i){
            var vue = this;
            this.headerText = this.playlists[i].playlist;
            this.modalPlaylistId = this.playlists[i].id;
            this.currentTab = this.playlists[i].id;
            this.songs = [];

            var id = this.playlists[i].id;
            axios.get('/displayPlaylistSongs/'+id).then(({ data })=> {
                for(i=0; i < data.length; i++){
                    vue.songs.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: 'none',
                        duration: vue.convertDuration(data[i].length),
                        playlist_songs_id: data[i].playlist_songs_id
                    });
                }
            }).catch((err)=> {});
        },
        saveToDatabase(title, artist, album, length, file){
            var vue = this;
            var duration = Math.round((length + Number.EPSILON) * 100) / 100;
            var formData = new FormData();
            formData.append('title', title);
            formData.append('artist', artist);
            formData.append('album', album);
            formData.append('length', duration);
            formData.append('song', file);
            axios.post('/addsong',
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(({ data })=> {
                vue.songs.push({
                    id: data,
                    title: title,
                    artist: artist,
                    album: album,
                    duration: vue.convertDuration(length)
                });
                console.log(data);
            }).catch((err)=> {});
        },
        getSongs(){
            this.headerText = "All Songs";
            this.modalPlaylistId = "";
            this.currentTab = "0";
            var vue = this;
            axios.get('/displaysong').then(({ data })=> {
                vue.songs = [];
                for(i=0; i < data.length; i++){
                    vue.songs.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: 'none',
                        duration: vue.convertDuration(data[i].length)
                    });
                }
               // console.log(vue.songs);
            })
            .catch((err)=> {});
        },
        getPlaylists(){
            var vue = this;
            axios.get('/displayplaylist').then(({ data })=> {
                for(i=0; i < data.length; i++){
                    vue.playlists.push({
                        id: data[i].id,
                        playlist: data[i].name
                    });
                }
            })
            .catch((err)=> {});
        }
    },
    computed: {
        resultQuery() {
            if (this.searchQuery) {
                return this.songs.filter((item) => {
                    return (this.searchQuery.toLowerCase().split(' ').every(v => item.title.toLowerCase().includes(v))) || (this.searchQuery.toLowerCase().split(' ').every(v => item.artist.toLowerCase().includes(v))) || (this.searchQuery.toLowerCase().split(' ').every(v => item.album.toLowerCase().includes(v))) 
                })
            } else {
                return this.songs;
            }
        }
    },
    beforeMount(){
        this.getSongs()
        this.getPlaylists()
     }
});
app.mount("#app")
