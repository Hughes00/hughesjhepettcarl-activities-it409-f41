const app = Vue.createApp({
    data() {
        return {
            sidebarToggled: false,
            headerText: "All Songs",
            myModal: null,
            modalName: "",
            modalHeader:"",
            modalButton:"",
            playlists: [
                {
                    playlist: "Playlist 1"
                },
                {
                    playlist: "Playlist 2"
                },
                {
                    playlist: "Playlist 3"
                }
            ],
            songs: [
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                {
                    title: "Some title here", artist: "Some artist here", album: "Some album here", duration: "3:14"
                },
                
            ]
        };
    },
    methods: {
        showSidebar(){
            this.sidebarToggled = true;
        },
        hideSidebar(){
            this.sidebarToggled = false;
        },
        adjustModal(modalName){
            this.modalName = modalName;
            if(modalName == "upload"){
                this.modalHeader = "Upload"
                this.modalButton = "Upload"
            }
            else if(modalName == "create"){
                this.modalHeader = "Create Playlist"
                this.modalButton = "Create"
            }
            this.myModal = new bootstrap.Modal(document.getElementById('upload-create-modal'), {})
            this.myModal.show()
        }
    },
});

app.mount("#app");

var myDropzone = new Dropzone("div#myId", {
    url: "/file/post"
});
